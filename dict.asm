section .text

global find_word

extern string_equals

; rdi - pointer to the key - string with null terminator
; rsi - the beginning of list
; returns: 
; rax - address of the element if one is found else - 0

find_word:
.loop:
	push rsi	
    
    .comparing:
	   add rsi, 8				
	   call string_equals	
       
	pop rsi		
    
    .checking_if_element_found:
	   cmp rax, 0 				
	   jne .found
    
    .go_to_next_elem:
        mov rsi, [rsi] 			
        
    .if_end_found:
        cmp rsi, 0 				
	    je .not_found 	
    
	jmp .loop 
.not_found:
    xor rax, rax 			 
    ret			
.found:
    mov rax, rsi 			
    ret 					

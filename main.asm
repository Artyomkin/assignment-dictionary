%define MAX_CAPACITY 256
%define OFFSET 8
%define STDERR 2
%define STDOUT 1

section .text

%include 'colon.inc'
%include 'lib.inc'
extern find_word

global _start

section .data

%include 'words.inc'

reading_error: db 'reading word error', 10, 0
found: db 'found entry: ', 0
not_found: db 'entry not found', 10, 0
enter_key: db 'key: ', 0

section .text

_start:

    .printing_invite:
        mov rsi, STDOUT				
        mov rdi, enter_key			
        call print_string	
        
    .buffer_allocation:
        sub rsp, MAX_CAPACITY		
        mov rdi, rsp				
        mov rsi, MAX_CAPACITY
        
    .reading_key:
        call read_word 	
        
    .error_checking:
        cmp rax, 0 					
        jz .err_read 	
        
    .searching:
        mov rsi, beginning_of_list_address;			
        mov rdi, rax 				
        call find_word	
        
    .existance_checking:
        cmp rax, 0 					
        je .not_found 				

.found:
    .shift:
   	    add rax, OFFSET 			
    push rax 					
    
    .address_to_rsi:
        mov rsi, rax				
        call string_length 			
        
    pop rsi
    add rax, rsi				
    inc rax 					
	push rax			
    
    .found_message:
	   mov rdi, found				 
	   mov rsi, STDOUT				 
	   call print_string 			 
       
    .element_output:
	   pop rdi 					
	   mov rsi, STDOUT				
	   call print_string 		 
	   call print_newline 			
       
	jmp .end					

.err_read:
	mov rsi, STDERR				
	mov rdi, reading_error			
	call print_string			
	jmp .end					

.not_found:
	mov rsi, STDERR				
	mov rdi, not_found			
	call print_string			
	jmp .end					

.end:
	add rsp, MAX_CAPACITY		
	call exit    				 
%define beginning_of_list_address 0

%macro colon 2											
	%ifid %2											
		%2: dq beginning_of_list_address									
		%define beginning_of_list_address %2						
	%else												
		%fatal "second argument must be an id" 		
	%endif													
	%ifstr %1											
		db %1, 0										
	%else												
		%fatal "The first argument must be a string"	
	%endif												 	
%endmacro	